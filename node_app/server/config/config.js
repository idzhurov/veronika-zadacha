const path = require('path')

let rootPath = path.normalize(path.join(__dirname, '../../'))

module.exports = {
  development: {
    rootPath: rootPath,
    db: 'mongodb://admin:admin@ds239097.mlab.com:39097/imoti-db-test',
    port: process.env.PORT || 1337
  },
  production: {}
}