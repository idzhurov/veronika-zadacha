const passport = require('passport')
const LocalPassport = require('passport-local')
const FacebookStrategy = require('passport-facebook').Strategy
const User = require('../data/User')

const facebookVariables = {
    clientID: '1741511899434837',
    clientSecret: 'c8925486e5ff1690f2c7901b5e414361',
    callbackURL: 'http://localhost/auth/facebook/callback'
  }

const authenticateUser = (username, password, done) => {
    User.findOne({ email: username }).then(user => {
        if (!user) {
            return done(null, false);
        }

        if (!user.authenticate(password)) {
            return done(null, false);
        }

        return done(null, user);
    });
};

module.exports = () => {
    passport.use(new LocalPassport({
        usernameField: 'email',
        passwordField: 'password'
    }, authenticateUser));

    passport.use(new FacebookStrategy({
        clientID: facebookVariables.clientID,
        clientSecret: facebookVariables.clientSecret,
        callbackURL: facebookVariables.callbackURL,
        profileFields: ['email', 'displayName']
    }, function (accessToken, refreshToken, profile, done) {
        User.findOne({ email: profile.emails[0].value }, (err, user) => {
            if (err) {
                console.log(err)
            }
            if (!err && user !== null) {
                done(null, user)
            } else {
                User.create({
                    email: profile.emails[0].value,
                    type: 'fb'
                }).then((user) => {
                    done(null, user)
                })
            }
        })
    }
    ))

    passport.serializeUser((user, done) => {
        if (!user) {
            return done(null, false);
        }

        return done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        User.findById(id).then((user) => {
            if (!user) {
                return done(null, false)
            }

            return done(null, user);
        })
    })
}